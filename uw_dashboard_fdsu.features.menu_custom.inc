<?php

/**
 * @file
 * uw_dashboard_fdsu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uw_dashboard_fdsu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-site-management.
  $menus['menu-site-management'] = array(
    'menu_name' => 'menu-site-management',
    'title' => 'Site management',
    'description' => 'Menu with options for Site Managers.',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Menu with options for Site Managers.');
  t('Site management');

  return $menus;
}
