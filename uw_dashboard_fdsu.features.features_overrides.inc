<?php
/**
 * @file
 * uw_dashboard_fdsu.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_dashboard_fdsu_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: views_view
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|table"] = 'views_entity_comment';
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::comment_save_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::comment_unpublish_by_keyword_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_goto_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_message_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_send_email_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_argument_selector_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_comment.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_script_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|table"] = 'views_entity_node';
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::node_save_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::node_unpublish_by_keyword_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_goto_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_message_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_send_email_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_argument_selector_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_node.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_script_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|table"] = 'views_entity_user';
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_block_ip_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_goto_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_message_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::user_block_user_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_argument_selector_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_delete_item"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_script_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|filters|name|expose|identifier"] = 'userid';
  $overrides["views_view.admin_views_user.display|default|display_options|filters|name|expose|label"] = 'UserID';
  $overrides["views_view.admin_views_user.display|default|display_options|filters|name|expose|remember"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|filters|name|expose|remember_roles"] = array(
    2 => 2,
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  $overrides["views_view.admin_views_user.display|default|display_options|filters|realname"] = array(
    'id' => 'realname',
    'table' => 'realname',
    'field' => 'realname',
    'operator' => 'contains',
    'group' => 1,
    'exposed' => TRUE,
    'expose' => array(
      'operator_id' => 'realname_op',
      'label' => 'Name',
      'operator' => 'realname_op',
      'identifier' => 'realname',
      'remember_roles' => array(
        2 => 2,
        1 => 0,
        21 => 0,
        16 => 0,
        19 => 0,
        13 => 0,
        14 => 0,
        15 => 0,
        3 => 0,
        6 => 0,
        12 => 0,
        5 => 0,
        4 => 0,
        7 => 0,
        8 => 0,
        9 => 0,
        11 => 0,
        10 => 0,
        18 => 0,
        20 => 0,
        17 => 0,
      ),
    ),
  );
  $overrides["views_view.admin_views_user.display|default|display_options|filters|rid_1"] = array(
    'id' => 'rid_1',
    'table' => 'users_roles',
    'field' => 'rid',
    'ui_name' => 'Role status',
    'group' => 1,
    'exposed' => TRUE,
    'expose' => array(
      'operator_id' => 'rid_1_op',
      'label' => 'Roles',
      'operator' => 'rid_1_op',
      'identifier' => 'rid_1',
      'multiple' => TRUE,
    ),
    'is_grouped' => TRUE,
    'group_info' => array(
      'label' => 'Role status',
      'identifier' => 'role_status',
      'default_group' => 1,
      'group_items' => array(
        1 => array(
          'title' => 'any roles',
          'operator' => 'not empty',
          'value' => array(
            'all' => 'all',
          ),
        ),
        2 => array(
          'title' => 'content editing roles',
          'operator' => 'or',
          'value' => array(
            3 => 3,
            6 => 6,
            12 => 12,
            5 => 5,
            4 => 4,
            7 => 7,
            9 => 9,
            11 => 11,
            10 => 10,
            18 => 18,
            20 => 20,
          ),
        ),
        3 => array(
          'title' => 'forms-related roles',
          'operator' => 'or',
          'value' => array(
            7 => 7,
            8 => 8,
          ),
        ),
        4 => array(
          'title' => 'no roles',
          'operator' => 'empty',
          'value' => array(
            'all' => 'all',
          ),
        ),
      ),
    ),
  );
  $overrides["views_view.admin_views_user.display|default|display_options|filters|rid|expose|remember_roles"] = array(
    2 => 2,
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  $overrides["views_view.admin_views_user.display|default|display_options|filters|rid|reduce_duplicates"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|group_by"] = TRUE;

 return $overrides;
}
