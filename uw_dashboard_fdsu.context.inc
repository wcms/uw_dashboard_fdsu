<?php

/**
 * @file
 * uw_dashboard_fdsu.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_dashboard_fdsu_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'dashboard_block_placement';
  $context->description = 'Block placement for dashboard blocks';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/dashboard' => 'admin/dashboard',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'node-recent' => array(
          'module' => 'node',
          'delta' => 'recent',
          'region' => 'dashboard_main',
          'weight' => '-10',
        ),
        'uw_whos_online-uw_online' => array(
          'module' => 'uw_whos_online',
          'delta' => 'uw_online',
          'region' => 'dashboard_sidebar',
          'weight' => '-20',
        ),
        'menu-menu-site-management' => array(
          'module' => 'menu',
          'delta' => 'menu-site-management',
          'region' => 'dashboard_sidebar',
          'weight' => '-15',
        ),
        'menu-menu-site-manager-vocabularies' => array(
          'module' => 'menu',
          'delta' => 'menu-site-manager-vocabularies',
          'region' => 'dashboard_sidebar',
          'weight' => '-10',
        ),
        'uw_content_type_use-uw_content_type_use' => array(
          'delta' => 'uw_content_type_use',
          'module' => 'uw_content_type_use',
          'region' => 'dashboard_sidebar',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Block placement for dashboard blocks');
  t('Content');
  $export['dashboard_block_placement'] = $context;

  return $export;
}
