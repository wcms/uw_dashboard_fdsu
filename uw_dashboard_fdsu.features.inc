<?php
/**
 * @file
 * uw_dashboard_fdsu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_dashboard_fdsu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_dashboard_fdsu_views_default_views_alter(&$data) {
  if (isset($data['admin_views_comment'])) {
    $data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_comment'; /* WAS: 'comment' */
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::comment_save_action']);
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::comment_unpublish_by_keyword_action']);
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_goto_action']);
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_message_action']);
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_send_email_action']);
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_argument_selector_action']);
    unset($data['admin_views_comment']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_script_action']);
  }
  if (isset($data['admin_views_node'])) {
    $data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_node'; /* WAS: 'node' */
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::node_save_action']);
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::node_unpublish_by_keyword_action']);
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_goto_action']);
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_message_action']);
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_send_email_action']);
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_argument_selector_action']);
    unset($data['admin_views_node']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_script_action']);
  }
  if (isset($data['admin_views_user'])) {
    $data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_user'; /* WAS: 'users' */
    $data['admin_views_user']->display['default']->display_options['filters']['name']['expose']['identifier'] = 'userid'; /* WAS: 'name' */
    $data['admin_views_user']->display['default']->display_options['filters']['name']['expose']['label'] = 'UserID'; /* WAS: 'Username' */
    $data['admin_views_user']->display['default']->display_options['filters']['name']['expose']['remember_roles'] = array(
      2 => 2,
      1 => 0,
      21 => 0,
      16 => 0,
      19 => 0,
      13 => 0,
      14 => 0,
      15 => 0,
      3 => 0,
      6 => 0,
      12 => 0,
      5 => 0,
      4 => 0,
      7 => 0,
      8 => 0,
      9 => 0,
      11 => 0,
      10 => 0,
      18 => 0,
      20 => 0,
      17 => 0,
    ); /* WAS: '' */
    $data['admin_views_user']->display['default']->display_options['filters']['realname'] = array(
      'id' => 'realname',
      'table' => 'realname',
      'field' => 'realname',
      'operator' => 'contains',
      'group' => 1,
      'exposed' => TRUE,
      'expose' => array(
        'operator_id' => 'realname_op',
        'label' => 'Name',
        'operator' => 'realname_op',
        'identifier' => 'realname',
        'remember_roles' => array(
          2 => 2,
          1 => 0,
          21 => 0,
          16 => 0,
          19 => 0,
          13 => 0,
          14 => 0,
          15 => 0,
          3 => 0,
          6 => 0,
          12 => 0,
          5 => 0,
          4 => 0,
          7 => 0,
          8 => 0,
          9 => 0,
          11 => 0,
          10 => 0,
          18 => 0,
          20 => 0,
          17 => 0,
        ),
      ),
    ); /* WAS: '' */
    $data['admin_views_user']->display['default']->display_options['filters']['rid_1'] = array(
      'id' => 'rid_1',
      'table' => 'users_roles',
      'field' => 'rid',
      'ui_name' => 'Role status',
      'group' => 1,
      'exposed' => TRUE,
      'expose' => array(
        'operator_id' => 'rid_1_op',
        'label' => 'Roles',
        'operator' => 'rid_1_op',
        'identifier' => 'rid_1',
        'multiple' => TRUE,
      ),
      'is_grouped' => TRUE,
      'group_info' => array(
        'label' => 'Role status',
        'identifier' => 'role_status',
        'default_group' => 1,
        'group_items' => array(
          1 => array(
            'title' => 'any roles',
            'operator' => 'not empty',
            'value' => array(
              'all' => 'all',
            ),
          ),
          2 => array(
            'title' => 'content editing roles',
            'operator' => 'or',
            'value' => array(
              3 => 3,
              6 => 6,
              12 => 12,
              5 => 5,
              4 => 4,
              7 => 7,
              9 => 9,
              11 => 11,
              10 => 10,
              18 => 18,
              20 => 20,
            ),
          ),
          3 => array(
            'title' => 'forms-related roles',
            'operator' => 'or',
            'value' => array(
              7 => 7,
              8 => 8,
            ),
          ),
          4 => array(
            'title' => 'no roles',
            'operator' => 'empty',
            'value' => array(
              'all' => 'all',
            ),
          ),
        ),
      ),
    ); /* WAS: '' */
    $data['admin_views_user']->display['default']->display_options['filters']['rid']['expose']['remember_roles'] = array(
      2 => 2,
      1 => 0,
      21 => 0,
      16 => 0,
      19 => 0,
      13 => 0,
      14 => 0,
      15 => 0,
      3 => 0,
      6 => 0,
      12 => 0,
      5 => 0,
      4 => 0,
      7 => 0,
      8 => 0,
      9 => 0,
      11 => 0,
      10 => 0,
      18 => 0,
      20 => 0,
      17 => 0,
    ); /* WAS: '' */
    $data['admin_views_user']->display['default']->display_options['filters']['rid']['reduce_duplicates'] = TRUE; /* WAS: '' */
    $data['admin_views_user']->display['default']->display_options['group_by'] = TRUE; /* WAS: '' */
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_block_ip_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_goto_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_message_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::user_block_user_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_argument_selector_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_delete_item']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_script_action']);
    unset($data['admin_views_user']->display['default']->display_options['filters']['name']['expose']['remember']);
  }

  // Manually added to force ordering. Make sure this stays if you are re-exporting the feature!
  if (isset($data['admin_views_user']->display['default']->display_options['filters'])) {
    // Define the desired order up to the point where the defaults are fine.
    $initial_order = array('uid', 'name', 'realname');
    // Create a new array to temporarily hold the reordered section or the original array.
    $new_array = array();
    foreach ($initial_order as $key) {
      $new_array[$key] = $data['admin_views_user']->display['default']->display_options['filters'][$key];
      // Remove the item from the original array.
      unset($data['admin_views_user']->display['default']->display_options['filters'][$key]);
    }
    // Redefine the original array as the combo of the new and original arrays.
    $data['admin_views_user']->display['default']->display_options['filters'] = array_merge($new_array, $data['admin_views_user']->display['default']->display_options['filters']);
  }
}
