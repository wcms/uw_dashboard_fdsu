<?php

/**
 * @file
 * uw_dashboard_fdsu.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_dashboard_fdsu_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_date_popup_minute_increment';
  $strongarm->value = 15;
  $export['scheduler_date_popup_minute_increment'] = $strongarm;

  return $export;
}
