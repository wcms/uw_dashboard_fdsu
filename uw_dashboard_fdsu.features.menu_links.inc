<?php

/**
 * @file
 * uw_dashboard_fdsu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_dashboard_fdsu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_body-content-search-report:admin/reports/uw_body_content_search_report.
  $menu_links['menu-site-management_body-content-search-report:admin/reports/uw_body_content_search_report'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/reports/uw_body_content_search_report',
    'router_path' => 'admin/reports/uw_body_content_search_report',
    'link_title' => 'Body content search report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_body-content-search-report:admin/reports/uw_body_content_search_report',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_broken-links:admin/reports/linkchecker.
  $menu_links['menu-site-management_broken-links:admin/reports/linkchecker'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/reports/linkchecker',
    'router_path' => 'admin/reports/linkchecker',
    'link_title' => 'Broken links',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_broken-links:admin/reports/linkchecker',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_content-report-csv:admin/reports/uw_content_report_csv.
  $menu_links['menu-site-management_content-report-csv:admin/reports/uw_content_report_csv'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/reports/uw_content_report_csv',
    'router_path' => 'admin/reports/uw_content_report_csv',
    'link_title' => 'Content report (CSV)',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_content-report-csv:admin/reports/uw_content_report_csv',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_menu-report-csv:admin/reports/uw_menu_report_csv.
  $menu_links['menu-site-management_menu-report-csv:admin/reports/uw_menu_report_csv'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/reports/uw_menu_report_csv',
    'router_path' => 'admin/reports/uw_menu_report_csv',
    'link_title' => 'Menu report (CSV)',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_menu-report-csv:admin/reports/uw_menu_report_csv',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_menus:admin/structure/menu.
  $menu_links['menu-site-management_menus:admin/structure/menu'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/structure/menu',
    'router_path' => 'admin/structure/menu',
    'link_title' => 'Menus',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_menus:admin/structure/menu',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_microsoft-teams-options:admin/config/system/notifications.
  $menu_links['menu-site-management_microsoft-teams-options:admin/config/system/notifications'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/notifications',
    'router_path' => 'admin/config/system/notifications',
    'link_title' => 'Microsoft Teams options',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_microsoft-teams-options:admin/config/system/notifications',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_path-review-report:admin/reports/uw_path_review.
  $menu_links['menu-site-management_path-review-report:admin/reports/uw_path_review'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/reports/uw_path_review',
    'router_path' => 'admin/reports',
    'link_title' => 'Path review report',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_path-review-report:admin/reports/uw_path_review',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_redirects:admin/config/search/redirect.
  $menu_links['menu-site-management_redirects:admin/config/search/redirect'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/search/redirect',
    'router_path' => 'admin/config/search/redirect',
    'link_title' => 'Redirects',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_redirects:admin/config/search/redirect',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_shortcuts:admin/config/user-interface/shortcut.
  $menu_links['menu-site-management_shortcuts:admin/config/user-interface/shortcut'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/user-interface/shortcut',
    'router_path' => 'admin/config/user-interface/shortcut',
    'link_title' => 'Shortcuts',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-management_shortcuts:admin/config/user-interface/shortcut',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-management_slackmattermost-options:admin/config/system/uw_slack.
  $menu_links['menu-site-management_slackmattermost-options:admin/config/system/uw_slack'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_slack',
    'router_path' => 'admin/config/system/uw_slack',
    'link_title' => 'Slack/Mattermost options',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_slackmattermost-options:admin/config/system/uw_slack',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_submit-request:mailto:https://uwaterloo.atlassian.net/servicedesk/customer/portal/117.
  $menu_links['menu-site-management_submit-request:https://uwaterloo.atlassian.net/servicedesk/customer/portal/117'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'https://uwaterloo.atlassian.net/servicedesk/customer/portal/117',
    'router_path' => '',
    'link_title' => 'Submit request',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_submit-request:https://uwaterloo.atlassian.net/servicedesk/customer/portal/117',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_wcms-how-to-documents:https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents.
  $menu_links['menu-site-management_wcms-how-to-documents:https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents',
    'router_path' => '',
    'link_title' => 'WCMS how-to documents',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_wcms-how-to-documents:https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body content search report');
  t('Broken links');
  t('Content report (CSV)');
  t('Menu report (CSV)');
  t('Menus');
  t('Microsoft Teams options');
  t('Path review report');
  t('Redirects');
  t('Shortcuts');
  t('Slack/Mattermost options');
  t('Submit request');
  t('WCMS how-to documents');

  return $menu_links;
}
